<?php 
namespace DarioRieke\DependencyInjection;

use Psr\Container\ContainerInterface;

/**
 * DependencyInjectionContainerInterface
 */
interface DependencyInjectionContainerInterface extends ContainerInterface {

	/**
	 * get a parameter from the container
	 * @param  string $name name of the parameter to get
	 * @throws DarioRieke\DependencyInjection\Exception\NotFoundException
	 * @return mixed
	 */
	public function getParameter(string $name);

	/**
	 * check if parameter exists
	 * @param string   $name name of the parameter
	 * @return boolean 
	 */
	public function hasParameter(string $name): bool;

	/**
	 * sets a parameter
	 * @param string $name  parameter name
	 * @param mixed  $value value to assign
	 */
	public function setParameter(string $name, $value);

	/**
	 * adds a singleton dependency to the container
	 * a dependency registered with this method must always return the same instance on multiple "get" calls
	 * @param  string   $name name of the parameter
	 * @param  callable $func callable which returns the instance
	 */
	public function singleton(string $name, callable $func);

	/**
	 * add a dependency to the container
	 * a dependency registered with this method must always return a new instance on multiple "get" calls
	 * @param  string   $name name of the parameter
	 * @param  callable $func callable which returns the instance
	 */
	public function register(string $name, callable $func);

	/**
	 * bind a string (commonly an interface name) to an existing dependency
	 * interface acts as an alias for the dependency
	 * used for resolving type hinted arguments 
	 * @param  string $dependencyName name of the dependency
	 * @param  string $aliasName      name of the alias
	 */
	public function alias(string $dependencyName, string $aliasName);

	/**
	 * 	add a method call on a dependency when its created, useful for example for setter methods
	 * @param string $name      name of the method
	 * @param string $method    method name
	 * @param array  $arguments arguments to pass to the method
	 */
	public function addMethodCall(string $name, string $method, array $arguments);
}

?>
