<?php 
declare(strict_types=1);

namespace DarioRieke\DependencyInjection\Tests\Fixtures;

class TestDependency {
	
	public $firstTestMethodCallCounter = 0;
	public $secondTestMethodCallCounter = 0;

	public function firstTestMethod($parameter) {
		$this->firstTestMethodCallCounter++;
	}

	public function secondTestMethod($parameter) {
		$this->secondTestMethodCallCounter++;
	}
}