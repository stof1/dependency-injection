<?php 
declare(strict_types=1);

namespace DarioRieke\DependencyInjection\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use DarioRieke\DependencyInjection\DependencyInjectionContainer;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\DependencyInjection\Exception\ContainerException;
use DarioRieke\DependencyInjection\Exception\NotFoundException;
use DarioRieke\DependencyInjection\Tests\Fixtures\TestDependency;

class DependencyInjectionContainerTest extends TestCase {

	/**
	 * @var DependencyInjectionContainer
	 */
	public $container; 
	
	public function setUp(): void {
		$this->container = new DependencyInjectionContainer();
	}

	public function testImplementsPsr11Interface() {
		$this->assertInstanceOf(ContainerInterface::class, $this->container);
	}

	public function testImplementsDependencyInjectionContainerInterface() {
		$this->assertInstanceOf(DependencyInjectionContainerInterface::class, $this->container);
	}

	public function testCanRegisterAndReturnSingletonDependency() {
		$dependency = $this->getTestDependency();

		$this->container->singleton('test', function ($container) use ($dependency) {
			return $dependency;
		});

		$this->assertTrue($this->container->has('test'));
		//test if get is same as reigstered value
		$this->assertSame($dependency, $this->container->get('test'));
		// test if it returns the same instance on following method calls
		$this->assertSame($this->container->get('test'), $this->container->get('test'));
	}

	public function testCanRegisterAndReturnDependency() {

		$this->container->register('test', function ($container) {
			return $this->getTestDependency();
		});

		$this->assertTrue($this->container->has('test'));
		// test if it returns a new instance on following method calls
		$this->assertEquals($this->container->get('test'), $this->container->get('test'));
		$this->assertNotSame($this->container->get('test'), $this->container->get('test'));

		return $this->container;
	}

	/**
	 * @depends testCanRegisterAndReturnDependency
	 */
	public function testCanReturnDependencyByAlias($container) {
		$container->alias('test', 'testInterface');
		$this->assertEquals($container->get('test'), $container->get('testInterface'));
	}

	/**
	 * @depends testCanRegisterAndReturnDependency
	 */
	public function testCanAddMethodCallToDependency($container) {
		$param1 = 'parameter';
		$param2 = 'parameter2';
		$container->addMethodCall('test', 'firstTestMethod', [$param1]);
		$container->addMethodCall('test', 'secondTestMethod', [$param2]);

		$dependency = $container->get('test');
		//both methods should be called once now
		$this->assertSame(1, $dependency->firstTestMethodCallCounter);
		$this->assertSame(1, $dependency->secondTestMethodCallCounter);
	}


	public function testCanAddMethodCallToSingletonDependency() {
		//we have to register the singelton again and cant use testCanRegisterAndReturnSingletonDependency as a data provider because after $container->get has been called once its always returning the same instance of the dependency
		
		$dependency = $this->getTestDependency();
		
		$this->container->singleton('test', function ($container) use ($dependency) {
			return $dependency;
		});

		$param1 = 'parameter';
		$param2 = 'parameter2';

		$this->container->addMethodCall('test', 'firstTestMethod', [$param1]);
		$this->container->addMethodCall('test', 'secondTestMethod', [$param2]);

		//both methods should be called once now
		$dependency = $this->container->get('test');
		$this->assertSame(1, $dependency->firstTestMethodCallCounter);
		$this->assertSame(1, $dependency->secondTestMethodCallCounter);

		//on following get calls the method calls should not be called again because its a singleton, the counter shouldnt increment
		$dependency = $this->container->get('test');

		$this->assertSame(1, $dependency->firstTestMethodCallCounter);
		$this->assertSame(1, $dependency->secondTestMethodCallCounter);
		
	}

	/**
	 * @dataProvider getParameters
	 */
	public function testCanGetAndSetParameter($param) {
		$this->container->setParameter('param', $param);

		$this->assertSame($param, $this->container->getParameter('param'));
	}

	public function testThrowsExceptionIfDependencyDoesNotExist() {
		$this->expectException(NotFoundException::class);
		$this->container->get('test');
	}

	public function testThrowsExceptionIfParameterDoesNotExist() {
		$this->expectException(NotFoundException::class);
		$this->container->getParameter('test');
	}

	public function testThrowsExceptionIfIdentifierForGetIsNotString() {
		$this->expectException(ContainerException::class);
		$this->container->get(3434);
	}

	public function testThrowsExceptionIfIdentifierForHasIsNotString() {
		$this->expectException(ContainerException::class);
		$this->container->has(3434);
	}


	/**
	 * @depends testCanRegisterAndReturnDependency
	 */
	public function testThrowsExceptionIfMethodToAddCallToIsNotFound($container) {
		$this->expectException(ContainerException::class);
		$container->addMethodCall('test', 'thisMethodDoesNotExist', []);

		$container->get('test');
	}

	public function testThrowsExceptionIfDependencyToAddMethodCallToDoesNotExist() {
		$this->expectException(NotFoundException::class);
		$this->container->addMethodCall('test', 'firstTestMethod', []);
	}

	protected function getTestDependency() {
		return new TestDependency;
	}

	/**
	 * data provider
	 */
	public function getParameters() {
		return [
			'int' 		=> [1234],
			'string' 	=> ['test'],
			'object' 	=> [new \stdClass()],
			'float' 	=> [23.4],
			'NULL' 		=> [NULL],
			'array' 	=> [array('test' => 1)]
		];
	}
}